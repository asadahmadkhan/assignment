﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class BookAlottedController : Controller
    {
        private Entities db = new Entities();

        // GET: /BookAlotted/
        public ActionResult Index()
        {
            var bookallotrecords = db.BookAllotRecords.Include(b => b.Book).Include(b => b.Student);
            return View(bookallotrecords.ToList());
        }

        // GET: /BookAlotted/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookAllotRecord bookallotrecord = db.BookAllotRecords.Find(id);
            if (bookallotrecord == null)
            {
                return HttpNotFound();
            }
            return View(bookallotrecord);
        }

        // GET: /BookAlotted/Create
        public ActionResult Create()
        {
            ViewBag.BookId = new SelectList(db.Books, "BookId", "BookName");
            ViewBag.StudentId = new SelectList(db.Students, "StudentId", "StudentName");
            return View();
        }

        // POST: /BookAlotted/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="BookAllotedID,StudentId,BookId")] BookAllotRecord bookallotrecord)
        {
            if (ModelState.IsValid)
            {
                db.BookAllotRecords.Add(bookallotrecord);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookId = new SelectList(db.Books, "BookId", "BookName", bookallotrecord.BookId);
            ViewBag.StudentId = new SelectList(db.Students, "StudentId", "StudentName", bookallotrecord.StudentId);
            return View(bookallotrecord);
        }

        // GET: /BookAlotted/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookAllotRecord bookallotrecord = db.BookAllotRecords.Find(id);
            if (bookallotrecord == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookId = new SelectList(db.Books, "BookId", "BookName", bookallotrecord.BookId);
            ViewBag.StudentId = new SelectList(db.Students, "StudentId", "StudentName", bookallotrecord.StudentId);
            return View(bookallotrecord);
        }

        // POST: /BookAlotted/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="BookAllotedID,StudentId,BookId")] BookAllotRecord bookallotrecord)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bookallotrecord).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookId = new SelectList(db.Books, "BookId", "BookName", bookallotrecord.BookId);
            ViewBag.StudentId = new SelectList(db.Students, "StudentId", "StudentName", bookallotrecord.StudentId);
            return View(bookallotrecord);
        }

        // GET: /BookAlotted/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookAllotRecord bookallotrecord = db.BookAllotRecords.Find(id);
            if (bookallotrecord == null)
            {
                return HttpNotFound();
            }
            return View(bookallotrecord);
        }

        // POST: /BookAlotted/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookAllotRecord bookallotrecord = db.BookAllotRecords.Find(id);
            db.BookAllotRecords.Remove(bookallotrecord);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
